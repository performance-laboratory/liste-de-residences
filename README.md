# Liste de résidences

Site Profile de ProcessWire

Développé dans le cadre du [CDP Performance Laboratory](https://performance.univ-grenoble-alpes.fr/).

## Fonctionnement

Des fonctions doivent être ajoutées avant la génération des pages. Il faut ajouter cette ligne dans le fichier `site/config.php`

```
$config->prependTemplateFile = '_init.php';
```

Le dossier `site/export` contient des fichiers pour déployer le site.


## Installation

- copier le git dans le dossier `site/templates` d'une installation ProcessWire (la faire avec un blank profile)
- importer la db `site/templates/_pl_list_residences_pw.sql` dans la table dédiée à l'installation
- installer les modules listés ci-dessous (fichiers dans le dossier `site/modules`)
- dans l'admin ProcessWire, dans la config du module RestApi (modules > site > RestApi), renseigner l'endpoint API souhaité ("liste_residences/api" si dans le sous-dossier "liste_residences" + si différent de /api à la fin, modifier dans `site/templates/scripts/main.js`)
- placer le dossier `site/templates/export/api/` dans `site/`
- dans `site/config.php` ajouter : `$config->prependTemplateFile = '_init.php';`
- dans `site/config.php` ajouter : `setlocale(LC_ALL,'en_US.UTF-8');` pour configurer les variables locales + ajouter fr_FR.UTF-8 dans l'interface de traduction si le site est multilingue

## Requirements

- droit d'écriture récursifs sur les dossiers `site/assets` et `site/modules`
- A Unix or Windows-based web server running Apache or compatible
- PHP version 5.4 or newer with PDO database support (PHP 7+ preferable)
- MySQL or MariaDB, 5.0.15 or greater (5.5+ preferable)
- Apache must have mod_rewrite enabled
- Apache must support .htaccess files
- PHP's bundled GD 2 library or ImageMagick library


## Modules

- [Leaflet Map Marker 2.8.1](https://modules.processwire.com/modules/fieldtype-leaflet-map-marker/) Field that stores an address with latitude and longitude coordinates and has built-in geocoding capability with Leaflet Maps API.
- [Export Site Profile 3.0.0](https://modules.processwire.com/modules/process-export-profile/) Create a site profile that can be used for a fresh install of ProcessWire
- [RestApi 0.0.7](https://modules.processwire.com/modules/rest-api/) Module to create a REST API with ProcessWire
- [Tracy Debugger 4.20.1](https://modules.processwire.com/modules/tracy-debugger/) Tracy debugger from Nette with several PW specific custom tools


## Licence

GNU Affero General Public License v3.0

---

[![Logo Performance Laboratory](export/img/logos/logo-pl.jpg "Lien vers le site web du Performance Laboratory")](https://performance.univ-grenoble-alpes.fr/)

[![Logo UGA](export/img/logos/logo-uga.jpg "Lien vers le site web de l'UGA")](https://www.univ-grenoble-alpes.fr/)

[![Logo Litt&Arts](export/img/logos/logo-littarts.jpg "Lien vers le site web de Litt&Arts")](https://litt-arts.univ-grenoble-alpes.fr/)