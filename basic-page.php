<?php include("inc/head.php"); ?>

<div class="container-fluid p-0">
	<div class="row no-gutters">

<aside class="col-12 col-md-2">
	<div class="p-3" data-simplebar>

		<h1><a href="<?= $homepage->url ?>"><?= $homepage->title ?></a></h1>

		<a id="login" href="<?= $pages->get(1)->httpUrl ?>processwire"><?=$login;?></a>

		<nav class="switcher mt-2">
        	<?php include("inc/lang_switcher.php"); ?>
		</nav>
	
	</div>
</aside>

<main class="py-3 pl-3 pr-3 pr-md-5">

	<div class="row">
		<header class="col-12 col-xxl-8 col-xxxl-6 mt-5">
			<?= $page->body ?>
		</header>
	</div>

	<footer class="mt-5">
		<?= $homepage->footer ?>
	</footer>

</main>

</div>

<?php include("inc/foot.php"); ?>