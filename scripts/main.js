document.addEventListener("DOMContentLoaded",  function() {
    init();
}, false);

function init() {

    // requête à l'API ProcessWire pour les données de leaflet    
    var main = document.querySelector("main");
    var api_host = main.getAttribute("data-api");
    if(api_host) ajaxCall(api_host);

    // event pour visualisation map
    var input_map = document.querySelector("#carte input");
    input_map.addEventListener('click', event => document.querySelector("#mapid").classList.toggle("visible"));

    // event pour les filtres location et type de résidences
    var filtres = document.querySelectorAll(".filtres input");
    for (var i = 0; i < filtres.length; i++) {
        filtres[i].addEventListener("click", filterList);
    }

    // toggle de l'opérateur conditionnel (&& ou ||) pour les filtres
    var conditions = document.querySelectorAll(".filtres .switcher span");
    for (var i = 0; i < conditions.length; i++) {
        conditions[i].addEventListener("click", toggleConditions);
    }

    // bouton tout ouvrir / fermer
    if (document.getElementById("toggleDetail")){
        var button_toggleDetail = document.querySelector("#toggleDetail");
        button_toggleDetail.addEventListener("click", toggleDetails);
    }

}


/*
    toggle de l'attribut "open" sur tous les <details> de la page
    appelé au click sur le button #toggleDetails
*/
function toggleDetails() {
    var details = document.querySelectorAll("details");
    if (this.classList.contains("active")) {
        details.forEach(el => el.setAttribute("open", true));
        this.classList.remove("active");
    } else {
        details.forEach(el => el.removeAttribute("open"));
        this.classList.add("active");
    }
}


// requête à l'API ProcessWire pour les données de leaflet
function ajaxCall(url) {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type','text/plain; charset=UTF-8');

    fetch(url, myHeaders)
        .then(function(response) { 
            return response.text();
        })
        .then(function(text) {
            var data = JSON.parse(text).residencies;
            initmap(data);
        })

}

// event pour les filtres location et type de résidences
// appelé à chaque fois qu'une checkbox est cliquée
function filterList() {

    var els = document.querySelectorAll('.residence');
    var els_map = document.querySelectorAll('#mapid .leaflet-marker-icon');
    
    var deja_selected = document.querySelectorAll('.filtres input:checked');
    
    var tableau_choix = [];
    for (var i = 0; i < deja_selected.length; i++) {
        tableau_choix.push(deja_selected[i].value);
    }

    console.log(tableau_choix);

    // reset 
    var el_deja_selected = document.querySelectorAll('.residence.selected');
    for (var i = 0; i < el_deja_selected.length; i++) el_deja_selected[i].classList.remove("selected");
    for (var i = 0; i < els_map.length; i++) els_map[i].classList.remove("selected");
    
    // collection des résultats
    var set_resultat = new Set();
    var set_res_map = new Set();

    var operator = document.querySelector(".filtres .switcher .active").getAttribute("data-operator");
    var ou = (operator === "or") ? true : false;

    // boucle dans la liste totale des rés pr la filtrée selon tableau_choix
    for (var j = 0; j < els.length; j++) {
        var cats = els[j].getAttribute("data-cat");

        if (ou) {
            var results = tableau_choix.some(function(element){
                var arr = cats.split(" ");
                return arr.indexOf(element) != -1;
            });
        } else {
            var results = tableau_choix.every(function(element){
                var arr = cats.split(" ");
                return arr.indexOf(element) != -1;
            });
        }

        if (results) {
            set_resultat.add(els[j]);
        }
    }

    // boucle ds la liste des rés de la map pr la filtrée selon tableau choix
    // tte les rés ne sont pas géoloc
    for (var j = 0; j < els_map.length; j++) {
        var cats = els_map[j].getAttribute("data-cat");
        if (ou) {
            var results = tableau_choix.some(function(element){
                var arr = cats.split(" ");
                return arr.indexOf(element) != -1;
            });
        } else {
            var results = tableau_choix.every(function(element){
                var arr = cats.split(" ");
                return arr.indexOf(element) != -1;
            });
        }

        if (results) {
            set_res_map.add(els_map[j]);
        }
    }

    // sélection des résultats front-end
    for (let item of set_resultat) item.classList.add("selected");
    for (let item of set_res_map) item.classList.add("selected");

    var deja_active = document.querySelectorAll("#res_list span.active");
    for (var i = 0; i < deja_active.length; i++) deja_active[i].classList.remove("active");
        
    // surligne les spans sélectionnés
    for (var i = 0; i < tableau_choix.length; i++) {
        var els = document.querySelectorAll("#res_list span[data-cat='" + tableau_choix[i] + "']");
        for (var j = 0; j < els.length; j++) els[j].classList.add("active");
    }

    var res_list = document.querySelector('#res_list');
    var map = document.querySelector('#mapid');
    if (tableau_choix.length) {
        res_list.classList.add("filtered");
        map.classList.add("filtered");

        // si aucun résultats
        console.log(set_resultat);
        if (!set_resultat.size) {
            console.log("sorry visible");
            var sorry = document.querySelector("#res_list .sorry");
            sorry.classList.add("visible");
        } else {
            console.log("sorry invisible");
            var sorry = document.querySelector("#res_list .sorry");
            sorry.classList.remove("visible");
        }
    } else {
        res_list.classList.remove("filtered");

        var sorry = document.querySelector("#res_list .sorry");
        sorry.classList.remove("visible");
    }

}


// toggle de l'opérateur conditionnel (&& ou ||) pour les filtres
// appelé par les buttons "and" et "or"
function toggleConditions() {
    document.querySelector(".filtres .switcher .active").classList.toggle("active");
    this.classList.toggle("active");
    // tri de la liste avec le changement de condition
    filterList();
}



// init de leaflet map
function initmap(data) {
    var mymap = L.map('mapid').setView([10, 0], 2.2);

    // https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoidm1haWxsYXJkIiwiYSI6ImNqc2VmNXdmdzE1cjU0NGw5eGZjMHdicnAifQ.IoSI4o23AwMGNj39tqRjAw
    // https://docs.mapbox.com/help/troubleshooting/migrate-legacy-static-tiles-api/?utm_campaign=email%7Cblast%7Cstudio%7Cstudio-classic-styles-deprecation-final-notice-20-10&utm_content=10%2F13%2F2020+-+Final+Notice+-+Studio+Classic+Styles+Deprecation&utm_medium=email_action&utm_source=customer.io#how-can-i-tell-if-my-style-is-a-classic-style-or-a-modern-mapbox-style
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 11,
        minZoom: 1,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.light',
        noWrap: true,
        zoomDelta: 0.5,
        closePopupOnClick: true
    }).addTo(mymap);

    var dataset = data;
    var greenIcon = L.icon({
        iconUrl: 'site/assets/files/img/marker-icon.png',
        
        iconSize: [20,27],
        iconAnchor: [10,23.5],
        popupAnchor:  [0, -20],
        slug: "salt"
        // iconSize:     [40, 47],
        // iconAnchor:   [40, 47]
    });
    
    
    // map sans cluster, sans control avec une possible gestion de catégories
    var markers = {};
    for (var i=0 ; i<dataset.length ; i++) {
        if (dataset[i].coordinates.length) {

            var uniq_id = dataset[i].id;

            var lat = dataset[i].coordinates[0];
            var lon = dataset[i].coordinates[1];

            var popup_link = document.createElement("A");
            popup_link.innerHTML = dataset[i].title;
            popup_link.href = "";
            popup_link.setAttribute("data-id", dataset[i].id);
            popup_link.addEventListener("click", scrollToRes);

            markers[uniq_id] = L.marker([lat, lon], {icon: greenIcon}).addTo(mymap).bindPopup(popup_link);
            markers[uniq_id]._icon.setAttribute("data-cat", dataset[i].datacats);            
        }
    }
    
    // map avec cluster, sans control
    // nécessite leaflet.markercluster installé
    /*
    var markers = L.markerClusterGroup();
    for (var i = 0; i < dataset.length; i++) {
        if (dataset[i].coordinates.length) {

            var lat = dataset[i].coordinates[0];
            var lon = dataset[i].coordinates[1];
            var marker = L.marker(
                new L.LatLng(lat, lon),
                { icon: greenIcon }
            )
            .bindPopup(dataset[i].title);
            markers.addLayer(marker);
        }
    }
    mymap.addLayer(markers);
    */

}

// click dans un lien d'une popup de la map
function scrollToRes(e) {
    e.preventDefault();
    var id = this.getAttribute("data-id");

    // fermeture panneau + uncheck checkbox
    document.querySelector("#carte input").checked = false;
    document.querySelector("#mapid").classList.toggle("visible");

    // scroll to la res en question + ouverture du details
    var res = document.querySelector(".residence[data-id='"+id+"']");
    window.scrollTo({ top: res.offsetTop - 20, behavior: 'smooth' });
    res.querySelector("details").setAttribute("open", "");
}