<?php include("inc/head.php"); ?>

<div class="container-fluid p-0">
	<div class="row no-gutters">

<aside class="col-12 col-md-2">
	<div class="p-3" data-simplebar>

		<h1><a href="<?= $homepage->url ?>"><?= $homepage->title ?></a></h1>

		<a id="login" href="<?= $pages->get(1)->httpUrl ?>processwire"><?=$login;?></a>

		<nav class="switcher mt-2">
        	<?php include("inc/lang_switcher.php"); ?>
		</nav>
	
		<div class="filtres_panneaux mt-2 mt-md-5">

			<h2 id="carte">Visualisation</h2>

		   	<label id="carte">
				<input autocomplete='off' type="checkbox" value="mapid"/>
		    	<span><?=$map?></span>
		   	</label>

	   	</div>


		<div class="filtres mt-2 mt-md-5">

			<h2><?=$filters?></h2>

			<div class="switcher mt-2">
				<span class="active p-1 mr-2" data-operator="and"><?=$and?></span>
				<span class="p-1 mr-2" data-operator="or"><?=$or?></span>
			</div>

			<h4 class="mt-2"><?=$res_location?></h4>
<!-- boucle dans les localisations institutionnelles des résidences -->
<?php foreach ($affiliations as $affiliation): ?>
			<label uk-tooltip="title: <?= $affiliation->body ?>; pos: right">
				<input autocomplete='off' name="employee" type="checkbox" value="<?=$affiliation->id?>"/>
				<span for=""><?=$affiliation->title?></span>
			</label>
<?php endforeach ?>

			<h4 class="mt-2"><?= $res_type ?></h4>
<!-- boucle dans les types de résidences -->
<?php foreach ($types as $type): ?>
			<label uk-tooltip="title: <?= $type->body ?>; pos: right">
				<input autocomplete='off' name="employee" type="checkbox" value="<?=$type->id?>"/>
				<span for=""><?=$type->title?></span>
			</label>
<?php endforeach ?>
		</div>

		<div class="mt-2 mt-md-5">
			<button id="toggleDetail" class="button active p-1 mr-2"><?= $toggle_details ?></button>
		</div>
	
	</div>

</aside>

<main class="col col-md-10 py-3 pl-3 pr-3 pr-md-5" data-api="<?= $api_host ?>">

	<div class="row">
		<header class="col-12 col-xxl-8 col-xxxl-6 mt-5">
			<?= $page->body ?>
		</header>
	</div>

	<div class="row" id="res_list">
	<p class="sorry"><?= $no_results ?></p>

	<!-- boucle dans toutes les résidences dans PW -->
	<?php foreach ($res_page->children as $res): ?>
	<div class="residence col-12 col-xxl-6 no-gutters" data-id="<?=$res->id?>" data-cat="<?=$res->categories->implode(" ", "id");?>">

	<details class="mb-4">
		<summary class="container-fluid p-4">
			<div class="row no-gutters">
				<div class="col-12 col-md-6">
					<h3><?= $res->title ?></h3>
				</div>

				<div class="col-8 col-md-4 tags_container categories">
					<?php foreach ($res->categories as $val): ?>
					<span class="pr-3 py-1"><?= $val->title ?></span>
					<?php endforeach ?>
				</div>

				<div class="col-4 col-md-2 tags_container lieux">
					<?php foreach ($res->countries as $val): ?>
					<span class="pr-3 py-1"><?= $val->title ?></span>
					<?php endforeach ?>
				</div>
			</div>
		</summary>
		
		<div class="content pt-4 pb-5 px-5">
			<article>
				<?= $res->body ?>

				<?php if(count($res->documents)): ?>
				<h4 class="mt-3">Documents</h4>
				<ul>
				<?php foreach($res->documents as $doc): ?>
					<li>
						<a href="<?= $doc->httpUrl() ?>" target="_blank"><?= $doc ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
				<?php endif; ?>

			</article>
		</div>

	</details>
	</div>
	<?php endforeach; ?>
	</div> <!--- fin #res_list -->

	<footer class="mt-5">
		<?= $page->footer ?>
		<p><a href="<?= $legal_page->url ?>"><?= $legal_page->title ?></a></p>
	</footer>

</main>

</div> <!-- fin container -->

<div id="mapid"></div>

<?php include("inc/foot.php"); ?>
