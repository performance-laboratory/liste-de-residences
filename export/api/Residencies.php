<?php namespace ProcessWire;

class Residencies {

  public static function test () {
    return 'test successful';
  }
  
  public static function getAll() {
    $response = new \StdClass();
    $response->residencies = [];
    $residencies_page  = wire('pages')->get("/residencies/");
    
    foreach($residencies_page->children as $key => $res) {
      // recup des coordonnées map si elles existent
      $cond = $res->leaflet_map->lat == "0.000000" && $res->leaflet_map->lng == "0.000000";
      $coordinates = ($cond) ? array() : array($res->leaflet_map->lat, $res->leaflet_map->lng);
      $datacats = $res->categories->implode(" ", "id");

      // construction de l'objet à renvoyer par résidence
      array_push($response->residencies, [
        "title" => $res->title->getLanguageValue("default"),
        "id" => $res->id,
        "coordinates" => $coordinates,
        "datacats" => $datacats,
      ]);
    }

    return $response;
  }

}