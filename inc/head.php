<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $page->title; ?></title>

	<!-- include leaflet -->
	<link rel="stylesheet" href="<?= $config->urls->templates?>lib/leaflet/leaflet.css" />
	<script src="<?= $config->urls->templates?>lib/leaflet/leaflet.js" defer></script>

	<!-- include leaflet markercluster -->
	<script src="<?= $config->urls->templates?>lib/leaflet.markercluster/leaflet.markercluster.js" defer></script>
	<link rel="stylesheet" href="<?= $config->urls->templates?>lib/leaflet.markercluster/MarkerCluster.css" />
	<link rel="stylesheet" href="<?= $config->urls->templates?>lib/leaflet.markercluster/MarkerCluster.Default.css" />

	<!-- include uikit -->
	<script src="<?= $config->urls->templates?>lib/uikit/uikit.min.js" defer></script>

	<!-- include simplebar -->
	<script src="<?= $config->urls->templates?>lib/simplebar/simplebar.min.js" defer></script>
	<link rel="stylesheet" href="<?= $config->urls->templates?>lib/simplebar/simplebar.min.css" />

	<!-- include bootstrap grid -->
	<link rel="stylesheet" href="<?= $config->urls->templates?>lib/bootstrap/bootstrap-grid.min.css" />

	<script src="<?= $config->urls->templates?>scripts/main.js" defer></script>

	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/uikit_custom.css" />
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/typo.css" />
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/map.css" />
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/bootstrap_extended.css" />
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/main.css" />

</head>

<body>