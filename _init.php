<?php

// translation
$login = __("Login");
$map = __("Map");
$filters = __("Filters");
$and = __("and");
$or = __("or");
$res_location = __("Institutional affiliation");
$res_type = __("Types of residency");
$toggle_details = __("Open / Close");
$no_results = __("No results");

// pw pages
$homepage = $pages->get("/");
$res_page = $pages->get("/residencies/");
$legal_page = $pages->get("/legal-disclaimer/");

// api host
$endpoint = wire('modules')->getModuleConfigData('RestApi')['endpoint'];
$httpHostUrl = $input->httpHostUrl();
$api_host = implode("/", array($httpHostUrl, $endpoint, 'residencies'));

$affiliations = $pages->get(1091)->children();
$types = $pages->get(1092)->children();

